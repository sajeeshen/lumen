<?php

namespace App\Http\Controllers;

use App\Repositories\Repository;
use App\Task;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;

class TaskController extends Controller
{
    use ApiResponser;

    protected $model;

    public function __construct(Task $task)
    {
        $this->model = new Repository($task);
    }

    public function index()
    {
        return $this->successResponse($this->model->all());
    }

    public function show($id)
    {
        return $this->successResponse($this->model->show($id));
    }

    public function store(Request $request)
    {
        return $this->model->create($request->only($this->model->getModel()->fillable));
    }

    public function update(Request $request, $id)
    {
        // update model and only pass in the fillable fields
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return $this->successResponse($this->model->show($id));
    }

    public function destroy($id)
    {
        return $this->model->delete($id);
    }

}
